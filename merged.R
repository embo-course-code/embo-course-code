# Read libraries
library(dplyr)
library(Seurat)
library(patchwork)
library(ggplot2)
library(cowplot)
library(glmGamPoi)

# Set the directories
main_path <- '~/biocluster4/scRNA-seq_data/'
data_path <- paste0(main_path, 'data/')
plot_path_merged <- paste0(main_path,'thesis/plots/merged')
rds_path_merged <- paste0(main_path,'thesis/rds/merged/')

# Load cell cycle genes
s.genes <- cc.genes$s.genes
g2m.genes <- cc.genes$g2m.genes

# Read data
all_patients <- readRDS(paste0(main_path,'thesis/rds/qc/all_patients_after_qc_doublets_removed.rds'))

# Get names of folders with data
patients <- list.files(path = data_path, pattern = '*')

# Merge into one Seurat object
all_patients_merged <- merge(all_patients[[1]], c(all_patients[[2]], all_patients[[3]], all_patients[[4]], all_patients[[5]], all_patients[[6]]), add.cell.ids = patients)

# Process the merged Seurat object
all_patients_merged <- NormalizeData(all_patients_merged)
all_patients_merged <- CellCycleScoring(object = all_patients_merged, s.features = s.genes, g2m.features = g2m.genes)
all_patients_merged <- SCTransform(all_patients_merged, method = "glmGamPoi", vst.flavor = "v2")
all_patients_merged <- RunPCA(all_patients_merged)
all_patients_merged <- RunUMAP(all_patients_merged, dims = 1:50
                               # , n.neighbors = 50, min.dist = 0.1
)

# Plot and save UMAPs; all organoids
merged_umap_organoid <- DimPlot(all_patients_merged, group.by = 'orig.ident')+theme(axis.title.x = element_blank())
ggsave(plot = merged_umap_organoid, filename = paste0('merged_umap_organoid_all_patients_default_umap.jpeg'), path = plot_path_merged, width = 11000, height = 11000, units = 'px', limitsize = FALSE, dpi = 1500)

merged_umap_phase <- DimPlot(all_patients_merged, group.by = 'Phase')+theme(axis.title.x = element_blank())
ggsave(plot = merged_umap_phase, filename = paste0('merged_umap_phase_all_patients_default_umap.jpeg'), path = plot_path_merged, width = 11000, height = 11000, units = 'px', limitsize = FALSE, dpi = 1500)

# Plot and save UMAPs; DD191 replicates
DD191_merged <- subset(all_patients_merged, subset = (orig.ident %in% c('DD191_2', 'DD191_4', 'DD191_5')))
merged_umap_phase_split_DD191 <- DimPlot(DD191_merged, group.by = 'Phase', split.by = 'orig.ident')+theme(axis.title.x = element_blank())
ggsave(plot = merged_umap_phase_split_DD191, filename = paste0('merged_umap_phase_split_all_patients_default_umap_DD191.jpeg'), path = plot_path_merged, width = 20000, height = 11000, units = 'px', limitsize = FALSE, dpi = 1500)

merged_umap_phase_split <- DimPlot(all_patients_merged, group.by = 'Phase', split.by = 'orig.ident')+theme(axis.title.x = element_blank())
ggsave(plot = merged_umap_phase_split, filename = paste0('merged_umap_phase_split_all_patients_default_umap.jpeg'), path = plot_path_merged, width = 30000, height = 11000, units = 'px', limitsize = FALSE, dpi = 1500)

# Get organoid specific markers 
all_patients_merged.markers <- FindAllMarkers(all_patients_merged, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
saveRDS(all_patients_merged.markers, paste0(rds_path_merged,'all_patients_merged_markers_per_patient.rds'))
all_patients_merged.markers <- all_patients_merged.markers[all_patients_merged.markers$pct.1 >= 0.25 & all_patients_merged.markers$pct.2 <= 0.9 & all_patients_merged.markers$p_val_adj < 0.001, ]

# Plot heatmap with organoid specific markers
all_patients_merged.markers %>%
  group_by(cluster) %>%
  top_n(n = 20, wt = avg_log2FC) -> top20
heat_map_merged_patient <- DoHeatmap(all_patients_merged, features = top20$gene) + NoLegend()
ggsave(plot = heat_map_merged_patient, filename = paste0('top20_heatmap_merged_all_patients_per_patient.jpeg'), path = plot_path_merged, width = 14000, height = 14000, units = 'px', dpi = 1500, limitsize = FALSE)

writeLines(top20$gene, paste0(rds_path_merged,'top20_genes_per_patient.txt'))

# Select features to plot
features_for_FeaturePlot <- c('KLK6', 'IGFL2', 'MDK', 'FABP1', 'MTRNR2L1', 'MSMB')

for (i in features_for_FeaturePlot){
  
  assign(i, FeaturePlot(all_patients_merged, features = i, min.cutoff = 'q10', max.cutoff = 'q99'))
  ggsave(plot = get(i), filename = paste0('Feature_plot_',i,'.jpeg'), path = plot_path_merged, width = 11000, height = 11000, units = 'px', limitsize = FALSE, dpi = 1500)
  
}

# Assemble the plot grid
Feature_plot_organoids <- plot_grid(KLK6, IGFL2, MDK, FABP1, MTRNR2L1, MSMB, nrow = 2)
ggsave(plot = Feature_plot_organoids, filename = paste0('Feature_plot_organoids.jpeg'), path = plot_path_merged, width = 20000, height = 10000, units = 'px', limitsize = FALSE, dpi = 1200)

saveRDS(all_patients_merged, paste0(rds_path_merged,'all_patients_merged.rds'))

# Cluster cells
all_patients_merged <- FindNeighbors(object = all_patients_merged,
                                     dims = 1:20)
all_patients_merged <- FindClusters(object = all_patients_merged, resolution = 0.05)

# Get cluster specific markers
all_patients_merged.markers <- FindAllMarkers(all_patients_merged, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
saveRDS(all_patients_merged.markers, paste0(rds_path_merged,'all_patients_merged_markers_per_cluster.rds'))
all_patients_merged.markers <- all_patients_merged.markers[all_patients_merged.markers$pct.1 >= 0.25 & all_patients_merged.markers$pct.2 <= 0.9 & all_patients_merged.markers$p_val_adj < 0.001, ]

# Plot heatmap with cluster specific markers
all_patients_merged.markers %>%
  group_by(cluster) %>%
  top_n(n = 20, wt = avg_log2FC) -> top20
heat_map_merged_cluster <- DoHeatmap(all_patients_merged, features = top20$gene) + NoLegend()
ggsave(plot = heat_map_merged_cluster, filename = paste0('top20_heat_map_merged_all_patients_cluster.jpeg'), path = plot_path_merged, width = 14000, height = 14000, units = 'px', dpi = 1500, limitsize = FALSE)
