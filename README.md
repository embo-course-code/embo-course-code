Here you can find snippets of my code:

- functions.R: Custom functions (plot and read data) for quality control (QC) steps.
- QC.R: Quality control steps: investigation and filtering the data.
- merged.R: First look at organoids after quality control step.
